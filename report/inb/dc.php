
<?php
       
    include('header.php');

    session_start();

    if(isset($_SESSION['username']))
    {
       $_SESSION['username'];
    }
    else
    {
        header("location:../../index.php");
    }


?> 

  <body class="bg-light">
    
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="#"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-files" viewBox="0 0 16 16">
  <path d="M13 0H6a2 2 0 0 0-2 2 2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h7a2 2 0 0 0 2-2 2 2 0 0 0 2-2V2a2 2 0 0 0-2-2zm0 13V4a2 2 0 0 0-2-2H5a1 1 0 0 1 1-1h7a1 1 0 0 1 1 1v10a1 1 0 0 1-1 1zM3 4a1 1 0 0 1 1-1h7a1 1 0 0 1 1 1v10a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V4z"/>
</svg>&nbsp;Dash Cam Pro</a>

  </div>
</nav>

<main class="container-fluid"><br><br>
  <div class="bg-light p-5 rounded">

    <div class="header container">
            <div class="row">
                <div class="col-lg-4">
                  <form action="" method="GET" class="" >
                    <div class="card shadow-lg border border-1">
                        <div class="m-2 p-2">
                            <div class="form-group">
                              <div class="row">
                                  <div class="col-md-5"> 
                                    <label for="from_date" class="p-2">Start Date</label>
                                  </div>
                                   <div class="col-md-7"> 
                                    <input type="date" name="from_date" value="<?php if(isset($_GET['from_date'])){echo $_GET['from_date'];} else{}  ?>" class="form-control" placeholder="From Date">
                                  </div>
                                </div>
                              </div>
                              <div class="form-group">
                                <div class="row">
                                  <div class="col-md-5"> 
                                    <label for="to_date" class="p-2">End Date</label>
                                  </div>
                                   <div class="col-md-7"> 
                                  <input type="date" name="to_date" value="<?php if(isset($_GET['to_date'])){echo $_GET['to_date'];} else{}  ?>" class="form-control" placeholder="From Date">
                                  </div>
                                </div>
                              </div><hr> 
                              <div class="form-group">  
                                 <div class="row">
                                  <div class="col-md-5"> 
                                  </div>
                                   <div class="col-md-7">
                                     <button type="submit" class="w-100 btn btn-outline-secondary bg-gradient" id="save">Search</button>
                                  </div>
                                </div>
                              </div>                       
                            
                        </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-4">
                  <div class="card  shadow-lg border border-1 pe-2 ps-2 pt-2">
                    <table class="table table-hover">
                                        <tbody>

                                          
                                              <tr>
                                                 <td colspan="2" class="text-dark text-center bg-white border-0">Campaign Minutes</td>    

                                              </tr>
                                              <?php
                                              $con = mysqli_connect("localhost", "root", "Altria123!@#", "adb");
                                              

                                          
                                              if(isset($_GET['from_date']) && isset($_GET['to_date'])){


                                                if(strtotime($_GET['from_date']) <= strtotime($_GET['to_date'])){

                                              

                                                $from_date = $_GET['from_date'];
                                                $to_date = $_GET['to_date'];

                                                
                                                  
                                                $sql = "SELECT SUM(actual_agent_talktime) as total FROM dc_tbl WHERE call_date BETWEEN '$from_date' AND '$to_date'";
                                                
                                    
                                                $sql_result = mysqli_query($con, $sql) or die (mysqli_error($con));


                                                if (mysqli_num_rows($sql_result) > 0 ) {

                                                  
                                                  while ($row = mysqli_fetch_assoc($sql_result)) {
                                                    ?>
                                                    <tr>
                                                      <td class="text-center text-dark bg-white border-1" >Total Minutes</td>                       
                                                      <td class="text-center text-dark bg-white border-1"><?php echo $row['total'];?></td>
                                                    </tr>

                                                    <?php
                                                  }





                                                }
                                                                                                                                    

                                              }
                                              else{
                                                ?>
                                                  <td><h5 class="text-white border-1"><?php echo "NO RESULT TOTAL MINS"; ?></h5></td>
                                                <?php
                                              }
                    } 

                                            ?>




                                    </tbody>  
                                </table>
                             
                          </div>
                    </div>
                <div class="col-lg-4">
                  
                </div>

            </div>
            <br><br>
      <div class="card shadow-lg border border-1">
        <div class="card-body">
              <table class="table table-bordered table-hover" style="font-size: 11px;">
                                         <thead>
                                              <tr class="bg-primary bg-gradient text-white text-center">
                                                  <th>Account ID</th>
                                                  <th>Call Date</th>
                                                  <th>Actual Agent talktime</th>
                                                  <th>Answered calls</th>
                                                  <th>Average Calltime in Sec</th>
                                                  <th>Average Calltime</th>
                                                  <th>Total Calltime</th>
                                                  <th>Total Calltime in Sec</th>
                                                  <th>Total Inbound Calls</th>
                                                  <th>Title</th>                           
                                              </tr>
                                          </thead>
                                          <tbody >
                                        
                                            <?php
                                              $con = mysqli_connect("localhost", "root", "Altria123!@#", "adb");
                                              

                                           
                                              if(isset($_GET['from_date']) && isset($_GET['to_date'])){


                                                if(strtotime($_GET['from_date']) <= strtotime($_GET['to_date'])){

                                              

                                                $from_date = $_GET['from_date'];
                                                $to_date = $_GET['to_date'];

                                                
                                                  
                                                $sql = "SELECT * FROM dc_tbl WHERE call_date BETWEEN '$from_date' AND '$to_date' ORDER BY call_date ASC";
                                                
                                    
                                                $sql_run = mysqli_query($con, $sql) or die (mysqli_error($con));


                                                if (mysqli_num_rows($sql_run) > 0 ) {

                                         
                                                  foreach ($sql_run as $row) {
                                                    ?>
                                                      <tr>
                                                              <td><?php echo $row['account_id'];?></td>
                                                              <td><?php echo $row['call_date'];?></td>
                                                              <td><?php echo $row['actual_agent_talktime'];?></td>
                                                              <td><?php echo $row['answered_calls'];?></td>
                                                              <td><?php echo $row['average_calltime_in_sec'];?></td>
                                                              <td><?php echo $row['average_calltime'];?></td>
                                                              <td><?php echo $row['total_calltime'];?></td>
                                                              <td><?php echo $row['total_calltime_in_sec'];?></td>
                                                              <td><?php echo $row['total_inbound_calls'];?></td>
                                                              <td><?php echo $row['title'];?></td>
                                                            </tr>
                                                       <?php     
                                                  
                                                  }
                                                  
                                                }
                                                                                                                                    
                                                else
                                                {
                                                  ?>
                                                    <td colspan="10"><h5><?php echo "NO API DATA FOUND!"; ?></h5></td>
                                                  <?php
                                                }

                                              }
                                              else{
                                                ?>
                                                  <td colspan="10"><h5><?php echo "From-date is Greater than To-Date or No filter select date. Please change it!"; ?></h5></td>
                                                <?php
                                              }
                    } 

                                            ?>

                                          
                                          </tbody>
                                    
                                </table>
                             </div>
                         </div>



    





  </div>
</main>


    <script src="../../bootstrap/js/bootstrap.bundle.min.js"></script>

      
  </body>
</html>
