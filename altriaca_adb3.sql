-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 21, 2022 at 11:18 PM
-- Server version: 10.2.41-MariaDB-cll-lve
-- PHP Version: 7.3.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `altriaca_adb3`
--

-- --------------------------------------------------------

--
-- Table structure for table `ap_tbl`
--

CREATE TABLE `ap_tbl` (
  `ap_id` int(11) NOT NULL,
  `call_date` date NOT NULL,
  `agent_name` varchar(50) NOT NULL,
  `account_name` varchar(50) NOT NULL,
  `hours` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `bh_tbl`
--

CREATE TABLE `bh_tbl` (
  `bh_id` int(11) NOT NULL,
  `call_date` date NOT NULL,
  `agent_name` varchar(50) NOT NULL,
  `account_name` varchar(50) NOT NULL,
  `hours` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `hl_tbl`
--

CREATE TABLE `hl_tbl` (
  `hl_id` int(11) NOT NULL,
  `call_date` date NOT NULL,
  `agent_name` varchar(50) NOT NULL,
  `account_name` varchar(50) NOT NULL,
  `hours` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `lc_tbl`
--

CREATE TABLE `lc_tbl` (
  `lc_id` int(11) NOT NULL,
  `call_date` date NOT NULL,
  `agent_name` varchar(50) NOT NULL,
  `account_name` varchar(50) NOT NULL,
  `hours` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lc_tbl`
--

INSERT INTO `lc_tbl` (`lc_id`, `call_date`, `agent_name`, `account_name`, `hours`) VALUES
(3, '2021-08-09', 'Jeric Nojor', 'LAE & CAC', 2),
(4, '2021-08-10', 'Jeric Nojor', 'LAE & CAC', 2),
(5, '2021-08-11', 'Jeric Nojor', 'LAE & CAC', 2),
(6, '2021-08-12', 'Jeric Nojor', 'LAE & CAC', 2),
(7, '2021-08-13', 'Jeric Nojor', 'LAE & CAC', 2),
(8, '2021-08-02', 'Jeric Nojor', 'LAE & CAC', 2),
(9, '2021-08-03', 'Jeric Nojor', 'LAE & CAC', 2),
(10, '2021-08-04', 'Jeric Nojor', 'LAE & CAC', 2),
(11, '2021-08-05', 'Jeric Nojor', 'LAE & CAC', 2),
(12, '2021-08-06', 'Jeric Nojor', 'LAE & CAC', 2),
(13, '2021-08-16', 'Jeric Nojor', 'LAE & CAC', 2),
(14, '2021-08-17', 'Jeric Nojor', 'LAE & CAC', 2),
(15, '2021-08-18', 'Jeric Nojor', 'LAE & CAC', 2),
(16, '2021-08-19', 'Jeric Nojor', 'LAE & CAC', 2),
(17, '2021-08-20', 'Jeric Nojor', 'LAE & CAC', 2),
(18, '2021-08-21', 'Jeric Nojor', 'LAE & CAC', 2),
(19, '2021-08-22', 'Jeric Nojor', 'LAE & CAC', 2),
(20, '2021-08-23', 'Jeric Nojor', 'LAE & CAC', 2),
(21, '2021-08-24', 'Jeric Nojor', 'LAE & CAC', 2),
(22, '2021-08-25', 'Jeric Nojor', 'LAE & CAC', 2),
(23, '2021-08-26', 'Jeric Nojor', 'LAE & CAC', 2),
(24, '2021-08-27', 'Jeric Nojor', 'LAE & CAC', 2),
(25, '2021-08-28', 'Jam Reyes', 'LAE & CAC', 2),
(26, '2021-08-29', 'Jam Reyes', 'LAE & CAC', 2),
(27, '2021-08-30', 'Anthony Castro', 'LAE & CAC', 2),
(28, '2021-08-31', 'Anthony Castro', 'LAE & CAC', 2),
(29, '2021-09-01', 'Anthony Castro', 'LAE & CAC', 2),
(30, '2021-09-02', 'Anthony Castro', 'LAE & CAC', 2),
(31, '2021-09-03', 'Jeric Nojor', 'LAE & CAC', 2),
(32, '2021-09-04', 'Jeric Nojor', 'LAE & CAC', 2),
(33, '2021-09-05', 'Jeric Nojor', 'LAE & CAC', 2),
(34, '2021-09-06', 'Jeric Nojor', 'LAE & CAC', 2),
(35, '2021-09-07', 'Jeric Nojor', 'LAE & CAC', 2),
(36, '2021-09-08', 'Jeric Nojor', 'LAE & CAC', 2),
(37, '2021-09-09', 'Jeric Nojor', 'LAE & CAC', 2),
(38, '2021-09-10', 'Jeric Nojor', 'LAE & CAC', 2),
(39, '2021-09-11', 'Anthony Castro', 'LAE & CAC', 2),
(40, '2021-09-12', 'Anthony Castro', 'LAE & CAC', 2),
(41, '2021-09-13', 'Anthony Castro', 'LAE & CAC', 2),
(42, '2021-09-14', 'Anthony Castro', 'LAE & CAC', 2),
(43, '2021-09-15', 'Anthony Castro', 'LAE & CAC', 2),
(44, '2021-09-16', 'Anthony Castro', 'LAE & CAC', 3),
(45, '2021-09-17', 'Anthony Castro', 'LAE & CAC', 2),
(46, '2021-09-18', 'Anthony Castro', 'LAE & CAC', 2),
(47, '2021-09-19', 'Anthony Castro', 'LAE & CAC', 2),
(48, '2021-09-20', 'Anthony Castro', 'LAE & CAC', 2),
(49, '2021-09-21', 'Anthony Castro', 'LAE & CAC', 2),
(50, '2021-09-22', 'Anthony Castro', 'LAE & CAC', 2),
(51, '2021-09-23', 'Anthony Castro', 'LAE & CAC', 2),
(52, '2021-09-24', 'Anthony Castro', 'LAE & CAC', 2),
(53, '2021-09-25', 'Jeric Nojor', 'LAE & CAC', 2),
(54, '2021-09-26', 'Jeric Nojor', 'LAE & CAC', 2),
(55, '2021-09-27', 'Anthony Castro', 'LAE & CAC', 2),
(56, '2021-09-28', 'Anthony Castro', 'LAE & CAC', 2),
(57, '2021-09-29', 'Anthony Castro', 'LAE & CAC', 2),
(58, '2021-09-30', 'Anthony Castro', 'LAE & CAC', 2),
(59, '2021-10-01', 'Anthony Castro', 'LAE & CAC', 2),
(60, '2021-10-02', 'Jeric Nojor', 'LAE & CAC', 2),
(61, '2021-10-03', 'Jeric Nojor', 'LAE & CAC', 2),
(62, '2021-10-04', 'Anthony Castro', 'LAE & CAC', 2),
(63, '2021-10-05', 'Anthony Castro', 'LAE & CAC', 2),
(64, '2021-10-06', 'Anthony Castro', 'LAE & CAC', 2),
(65, '2021-10-07', 'Anthony Castro', 'LAE & CAC', 2),
(66, '2021-10-08', 'Anthony Castro', 'LAE & CAC', 2),
(67, '2021-10-09', 'Jeric Nojor', 'LAE & CAC', 2),
(68, '2021-10-10', 'Jeric Nojor', 'LAE & CAC', 2),
(69, '2021-10-12', 'Anthony Castro', 'LAE & CAC', 2),
(70, '2021-10-13', 'Anthony Castro', 'LAE & CAC', 2),
(71, '2021-10-14', 'Anthony Castro', 'LAE & CAC', 2),
(72, '2021-10-15', 'Anthony Castro', 'LAE & CAC', 4),
(73, '2021-10-16', 'jeric', 'LAE & CAC', 2),
(74, '2021-10-17', 'Jeric Nojor', 'LAE & CAC', 2),
(75, '2021-10-18', 'Jeric Nojor', 'LAE & CAC', 2),
(76, '2021-10-19', 'Jeric Nojor', 'LAE & CAC', 2),
(77, '2021-10-20', 'Jeric Nojor', 'LAE & CAC', 2),
(78, '2021-10-21', 'Jeric Nojor', 'LAE & CAC', 2),
(79, '2021-10-22', 'Jeric Nojor', 'LAE & CAC', 2),
(80, '2021-10-23', 'Anthony Castro', 'LAE & CAC', 2),
(81, '2021-10-24', 'Anthony Castro', 'LAE & CAC', 2),
(82, '2021-10-25', 'Anthony Castro', 'LAE & CAC', 2),
(83, '2021-10-26', 'Anthony Castro', 'LAE & CAC', 2),
(84, '2021-10-27', 'Anthony Castro', 'LAE & CAC', 2),
(85, '2021-10-28', 'Anthony Castro', 'LAE & CAC', 2),
(86, '2021-10-29', 'Anthony Castro', 'LAE & CAC', 2),
(87, '2021-10-30', 'Jeric Nojor', 'LAE & CAC', 2),
(88, '2021-10-31', 'Jeric Nojor', 'LAE & CAC', 2),
(89, '2021-11-01', 'Anthony Castro', 'LAE & CAC', 2),
(90, '2021-11-02', 'Anthony Castro', 'LAE & CAC', 2),
(91, '2021-11-03', 'Anthony Castro', 'LAE & CAC', 2),
(92, '2021-11-04', 'Anthony Castro', 'LAE & CAC', 2),
(93, '2021-11-05', 'Anthony Castro', 'LAE & CAC', 2),
(94, '2021-11-06', 'Jeric Nojor', 'LAE & CAC', 2),
(95, '2021-11-07', 'Jeric Nojor', 'LAE & CAC', 2),
(96, '2021-11-08', 'Anthony Castro', 'LAE & CAC', 2),
(97, '2021-11-09', 'Anthony Castro', 'LAE & CAC', 2),
(98, '2021-11-10', 'Anthony Castro', 'LAE & CAC', 2),
(99, '2021-11-11', 'Anthony Castro', 'LAE & CAC', 2),
(100, '2021-11-12', 'Anthony Castro', 'LAE & CAC', 2),
(101, '2021-11-13', 'Jeric Nojor', 'LAE & CAC', 2),
(102, '2021-11-14', 'Jeric Nojor', 'LAE & CAC', 2),
(103, '2021-11-15', 'Anthony Castro', 'LAE & CAC', 2),
(104, '2021-11-16', 'Anthony Castro', 'LAE & CAC', 2),
(105, '2021-11-17', 'Anthony Castro', 'LAE & CAC', 2),
(106, '2021-11-18', 'Anthony Castro', 'LAE & CAC', 2),
(107, '2021-11-19', 'Anthony Castro', 'LAE & CAC', 2),
(108, '2021-11-20', 'Anthony Castro', 'LAE & CAC', 2),
(109, '2021-11-21', 'Anthony Castro', 'LAE & CAC', 2),
(110, '2021-11-22', 'Anthony Castro', 'LAE & CAC', 2),
(111, '2021-11-23', 'Anthony Castro', 'LAE & CAC', 2),
(112, '2021-11-24', 'Anthony Castro', 'LAE & CAC', 2),
(113, '2021-11-26', 'Anthony Castro', 'LAE & CAC', 2),
(114, '2021-11-27', 'Anthony Castro', 'LAE & CAC', 2),
(115, '2021-11-28', 'Anthony Castro', 'LAE & CAC', 2),
(116, '2021-11-29', 'Anthony Castro', 'LAE & CAC', 2),
(117, '2021-11-30', 'Anthony Castro', 'LAE & CAC', 2),
(118, '2021-12-01', 'Anthony Castro', 'LAE & CAC', 2),
(119, '2021-12-02', 'Anthony Castro', 'LAE & CAC', 2),
(120, '2021-12-03', 'Anthony Castro', 'LAE & CAC', 2),
(121, '2021-12-04', 'Anthony Castro', 'LAE & CAC', 2),
(122, '2021-12-05', 'Anthony Castro', 'LAE & CAC', 2),
(123, '2021-12-06', 'Anthony Castro', 'LAE & CAC', 2),
(124, '2021-12-07', 'Anthony Castro', 'LAE & CAC', 2),
(125, '2021-12-08', 'Anthony Castro', 'LAE & CAC', 2),
(126, '2021-12-09', 'Anthony Castro', 'LAE & CAC', 2),
(127, '2021-12-10', 'Anthony Castro', 'LAE & CAC', 2),
(128, '2021-12-11', 'Anthony Castro', 'LAE & CAC', 2),
(129, '2021-12-12', 'Anthony Castro', 'LAE & CAC', 2),
(130, '2021-12-13', 'Anthony Castro', 'LAE & CAC', 2),
(131, '2021-12-14', 'Anthony Castro', 'LAE & CAC', 2),
(132, '2021-12-15', 'Anthony Castro', 'LAE & CAC', 2),
(133, '2021-12-16', 'Anthony Castro', 'LAE & CAC', 2),
(134, '2021-12-17', 'Anthony Castro', 'LAE & CAC', 2),
(135, '2021-12-18', 'Anthony Castro', 'LAE & CAC', 2),
(136, '2021-12-19', 'Anthony Castro', 'LAE & CAC', 2),
(137, '2021-12-20', 'Anthony Castro', 'LAE & CAC', 2),
(138, '2021-12-21', 'Anthony Castro', 'LAE & CAC', 2),
(139, '2021-12-22', 'Anthony Castro', 'LAE & CAC', 2),
(140, '2021-12-23', 'Anthony Castro', 'LAE & CAC', 2),
(141, '2021-12-27', 'Anthony Castro', 'LAE & CAC', 2),
(142, '2021-12-28', 'Anthony Castro', 'LAE & CAC', 2),
(143, '2021-12-29', 'Anthony Castro', 'LAE & CAC', 2),
(144, '2021-12-30', 'Anthony Castro', 'LAE & CAC', 2),
(145, '2022-01-03', 'Anthony Castro', 'LAE & CAC', 2),
(146, '2022-01-04', 'Anthony Castro', 'LAE & CAC', 2),
(147, '2022-01-05', 'Anthony Castro', 'LAE & CAC', 2),
(148, '2022-01-06', 'Anthony Castro', 'LAE & CAC', 2),
(149, '2022-01-07', 'Anthony Castro', 'LAE & CAC', 2),
(150, '2022-01-08', 'Anthony Castro', 'LAE & CAC', 2),
(151, '2022-01-09', 'Anthony Castro', 'LAE & CAC', 2),
(152, '2022-01-10', 'Anthony Castro', 'LAE & CAC', 2),
(153, '2022-01-11', 'Anthony Castro', 'LAE & CAC', 2),
(154, '2022-01-12', 'Anthony Castro', 'LAE & CAC', 2),
(155, '2022-01-13', 'Anthony Castro', 'LAE & CAC', 2),
(156, '2022-01-14', 'Anthony Castro', 'LAE & CAC', 2),
(157, '2022-01-15', 'Anthony Castro', 'LAE & CAC', 2),
(158, '2022-01-16', 'Anthony Castro', 'LAE & CAC', 2);

-- --------------------------------------------------------

--
-- Table structure for table `pt_tbl`
--

CREATE TABLE `pt_tbl` (
  `pt_id` int(11) NOT NULL,
  `call_date` date NOT NULL,
  `agent_name` varchar(50) NOT NULL,
  `account_name` varchar(50) NOT NULL,
  `hours` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pt_tbl`
--

INSERT INTO `pt_tbl` (`pt_id`, `call_date`, `agent_name`, `account_name`, `hours`) VALUES
(3, '2021-08-13', 'Altria', 'Patriots', 8),
(4, '2021-08-20', 'Altria', 'Patriots', 8),
(5, '2021-09-06', 'Altria', 'Patriots', 4),
(6, '2021-09-07', 'Altria', 'Patriots', 2),
(7, '2021-09-30', 'Altria', 'Patriots', 2);

-- --------------------------------------------------------

--
-- Table structure for table `qi_tbl`
--

CREATE TABLE `qi_tbl` (
  `qi_id` int(11) NOT NULL,
  `call_date` date NOT NULL,
  `agent_name` varchar(50) NOT NULL,
  `account_name` varchar(50) NOT NULL,
  `hours` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sgf_tbl`
--

CREATE TABLE `sgf_tbl` (
  `sgf_id` int(11) NOT NULL,
  `call_date` date NOT NULL,
  `agent_name` varchar(50) NOT NULL,
  `account_name` varchar(50) NOT NULL,
  `hours` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sg_tbl`
--

CREATE TABLE `sg_tbl` (
  `sg_id` int(11) NOT NULL,
  `call_date` date NOT NULL,
  `agent_name` varchar(50) NOT NULL,
  `account_name` varchar(50) NOT NULL,
  `hours` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sg_tbl`
--

INSERT INTO `sg_tbl` (`sg_id`, `call_date`, `agent_name`, `account_name`, `hours`) VALUES
(13, '2021-08-02', 'Altria', 'Shen Group', 8),
(14, '2021-08-03', 'Altria', 'Shen Group', 8),
(15, '2021-08-04', 'Altria', 'Shen Group', 8),
(16, '2021-08-05', 'Altria', 'Shen Group', 8),
(17, '2021-08-06', 'Altria', 'Shen Group', 8),
(18, '2021-08-09', 'Altria', 'Shen Group', 8),
(19, '2021-08-10', 'Altria', 'Shen Group', 8),
(20, '2021-08-11', 'Altria', 'Shen Group', 8),
(21, '2021-08-12', 'Altria', 'Shen Group', 8),
(22, '2021-08-16', 'Altria', 'Shen Group', 8),
(23, '2021-08-17', 'Altria', 'Shen Group', 8),
(24, '2021-08-18', 'Altria', 'Shen Group', 8),
(25, '2021-08-19', 'Altria', 'Shen Group', 8),
(26, '2021-08-23', 'Altria', 'Shen Group', 19),
(27, '2021-08-31', 'Altria', 'Shen Group', 21),
(28, '2021-09-01', 'Altria', 'Shen Group', 2),
(29, '2021-09-02', 'Altria', 'Shen Group', 9),
(31, '2021-09-06', 'Altria', 'Shen Group', 9),
(32, '2021-09-07', 'Altria', 'Shen Group', 14),
(33, '2021-09-08', 'Altria', 'Shen Group', 4),
(34, '2021-09-09', 'Altria', 'Shen Group', 2),
(35, '2021-09-10', 'Altria', 'Shen Group', 7),
(36, '2021-09-13', 'Altria', 'Shen Group', 11),
(37, '2021-09-13', 'Altria', 'Shen Group', 11),
(38, '2021-09-14', 'Altria', 'Shen Group', 6),
(39, '2021-09-15', 'Altria', 'Shen Group', 2),
(40, '2021-09-16', 'Altria', 'Shen Group', 7),
(41, '2021-09-17', 'Altria', 'Shen Group', 6),
(42, '2021-09-20', 'Altria', 'Shen Group', 2),
(43, '2021-09-21', 'Altria', 'Shen Group', 2),
(44, '2021-09-22', 'Altria', 'Shen Group', 3),
(45, '2021-09-23', 'Altria', 'Shen Group', 11),
(46, '2021-09-30', 'Altria', 'Shen Group', 6),
(47, '2021-09-29', 'Altria', 'Shen Group', 4),
(48, '2021-09-28', 'Altria', 'Shen Group', 7),
(57, '2021-12-03', 'Altria', 'Shen Group', 7);

-- --------------------------------------------------------

--
-- Table structure for table `si_tbl`
--

CREATE TABLE `si_tbl` (
  `si_id` int(11) NOT NULL,
  `call_date` date NOT NULL,
  `agent_name` varchar(50) NOT NULL,
  `account_name` varchar(50) NOT NULL,
  `hours` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ts_tbl`
--

CREATE TABLE `ts_tbl` (
  `ts_id` int(11) NOT NULL,
  `call_date` date NOT NULL,
  `agent_name` varchar(50) NOT NULL,
  `account_name` varchar(50) NOT NULL,
  `hours` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ts_tbl`
--

INSERT INTO `ts_tbl` (`ts_id`, `call_date`, `agent_name`, `account_name`, `hours`) VALUES
(3, '2021-08-02', 'Altria', 'TJ&S', 8),
(5, '2021-08-03', 'Altria', 'TJ&S', 8),
(6, '2021-08-04', 'Altria', 'TJ&S', 8),
(7, '2021-08-05', 'Altria', 'TJ&S', 8),
(8, '2021-08-06', 'Altria', 'TJ&S', 8),
(9, '2021-08-09', 'Altria', 'TJ&S', 8),
(10, '2021-08-10', 'Altria', 'TJ&S', 8),
(11, '2021-08-11', 'Altria', 'TJ&S', 8),
(12, '2021-08-12', 'Altria', 'TJ&S', 8),
(13, '2021-08-13', 'Altria', 'TJ&S', 8),
(14, '2021-08-16', 'Altria', 'TJ&S', 8),
(15, '2021-08-17', 'Altria', 'TJ&S', 8),
(16, '2021-08-18', 'Altria', 'TJ&S', 8),
(17, '2021-08-19', 'Altria', 'TJ&S', 8),
(18, '2021-08-20', 'Altria', 'TJ&S', 8),
(19, '2021-08-23', 'Altria', 'TJ&S', 8),
(20, '2021-08-24', 'Altria', 'TJ&S', 8),
(21, '2021-08-25', 'Altria', 'TJ&S', 4),
(22, '2021-08-26', 'Altria', 'TJ&S', 4),
(23, '2021-08-27', 'Altria', 'TJ&S', 4),
(24, '2021-08-30', 'Altria', 'TJ&S', 8),
(25, '2021-08-31', 'Altria', 'TJ&S', 8),
(26, '2021-09-03', 'Altria', 'TJ&S', 7),
(27, '2021-09-02', 'Altria', 'TJ&S', 6),
(28, '2021-09-01', 'Altria', 'TJ&S', 7),
(29, '2021-09-06', 'Altria', 'TJ&S', 5),
(30, '2021-09-07', 'Altria', 'TJ&S', 6),
(31, '2021-09-08', 'Altria', 'TJ&S', 5),
(32, '2021-09-09', 'Altria', 'TJ&S', 8),
(33, '2021-09-10', 'Altria', 'TJ&S', 6),
(34, '2021-09-13', 'Altria', 'TJ&S', 5),
(35, '2021-09-14', 'Altria', 'TJ&S', 7),
(36, '2021-09-15', 'Altria', 'TJ&S', 7),
(37, '2021-09-16', 'Altria', 'TJ&S', 5),
(38, '2021-09-17', 'Altria', 'TJ&S', 6),
(39, '2021-09-20', 'Altria', 'TJ&S', 4),
(40, '2021-09-21', 'Altria', 'TJ&S', 5),
(41, '2021-09-22', 'Altria', 'TJ&S', 5),
(42, '2021-09-23', 'Altria', 'TJ&S', 6),
(43, '2021-09-24', 'Altria', 'TJ&S', 5),
(44, '2021-09-27', 'Altria', 'TJ&S', 6),
(45, '2021-09-28', 'Altria', 'TJ&S', 5),
(46, '2021-09-29', 'Altria', 'TJ&S', 5),
(47, '2021-09-29', 'Altria', 'TJ&S', 6),
(48, '2021-09-30', 'Altria', 'TJ&S', 2),
(49, '2021-10-04', 'Altria', 'TJ&S', 4),
(50, '2021-10-05', 'Altria', 'TJ&S', 3),
(51, '2021-10-06', 'Altria', 'TJ&S', 5),
(52, '2021-10-07', 'Altria', 'TJ&S', 5),
(53, '2021-10-11', 'Altria', 'TJ&S', 5),
(54, '2021-10-12', 'Altria', 'TJ&S', 6),
(55, '2021-10-13', 'Altria', 'TJ&S', 4),
(56, '2021-10-14', 'Altria', 'TJ&S', 5),
(57, '2021-10-15', 'Altria', 'TJ&S', 4),
(58, '2021-10-18', 'Altria', 'TJ&S', 4),
(59, '2021-10-19', 'Altria', 'TJ&S', 5),
(60, '2021-10-20', 'Altria', 'TJ&S', 4),
(61, '2021-10-21', 'Altria', 'TJ&S', 4),
(62, '2021-10-22', 'Altria', 'TJ&S', 6),
(63, '2021-10-25', 'Altria', 'TJ&S', 4),
(64, '2021-10-26', 'Altria', 'TJ&S', 4),
(65, '2021-10-27', 'Altria', 'TJ&S', 3),
(66, '2021-10-28', 'Altria', 'TJ&S', 4),
(67, '2021-11-01', 'Altria', 'TJ&S', 4),
(68, '2021-11-02', 'Altria', 'TJ&S', 4),
(69, '2021-11-03', 'Altria', 'TJ&S', 5),
(70, '2021-11-04', 'Altria', 'TJ&S', 5),
(71, '2021-11-05', 'Altria', 'TJ&S', 4),
(72, '2021-11-08', 'Altria', 'TJ&S', 5),
(73, '2021-11-09', 'Altria', 'TJ&S', 5),
(74, '2021-11-10', 'Altria', 'TJ&S', 5),
(75, '2021-11-11', 'Altria', 'TJ&S', 4),
(76, '2021-11-12', 'Altria', 'TJ&S', 4),
(77, '2021-11-15', 'Altria', 'TJ&S', 5),
(78, '2021-11-16', 'Altria', 'TJ&S', 4),
(79, '2021-11-17', 'Altria', 'TJ&S', 4),
(80, '2021-11-18', 'Altria', 'TJ&S', 5),
(81, '2021-11-19', 'Altria', 'TJ&S', 5),
(82, '2021-11-22', 'Altria', 'TJ&S', 4),
(83, '2021-11-23', 'Altria', 'TJ&S', 4),
(84, '2021-11-24', 'Altria', 'TJ&S', 5),
(85, '2021-11-26', 'Altria', 'TJ&S', 4),
(86, '2021-11-29', 'Altria', 'TJ&S', 5),
(87, '2021-11-30', 'Altria', 'TJ&S', 5),
(88, '2021-12-01', 'Altria', 'TJ&S', 4),
(89, '2021-12-02', 'Altria', 'TJ&S', 5),
(90, '2021-12-03', 'Altria', 'TJ&S', 4),
(91, '2021-12-06', 'Altria', 'TJ&S', 4),
(92, '2021-12-07', 'Altria', 'TJ&S', 4),
(93, '2021-12-08', 'Altria', 'TJ&S', 5),
(94, '2021-12-09', 'Altria', 'TJ&S', 5),
(95, '2021-12-10', 'Altria', 'TJ&S', 3),
(96, '2021-12-13', 'Altria', 'TJ&S', 4),
(97, '2021-12-14', 'Altria', 'TJ&S', 4),
(98, '2021-12-15', 'Altria', 'TJ&S', 4),
(99, '2021-12-16', 'Altria', 'TJ&S', 4),
(100, '2021-12-17', 'Altria', 'TJ&S', 4),
(101, '2021-12-20', 'Altria', 'TJ&S', 4),
(102, '2021-12-21', 'Altria', 'TJ&S', 5),
(103, '2021-12-22', 'Altria', 'TJ&S', 5),
(104, '2021-12-23', 'Altria', 'TJ&S', 4),
(105, '2021-12-27', 'Altria', 'TJ&S', 5),
(106, '2021-12-28', 'Altria', 'TJ&S', 4),
(107, '2021-12-27', 'Altria', 'TJ&S', 5),
(108, '2021-12-28', 'Altria', 'TJ&S', 4),
(109, '2021-12-29', 'Altria', 'TJ&S', 4),
(110, '2021-12-30', 'Altria', 'TJ&S', 4),
(111, '2022-01-03', 'Altria', 'TJ&S', 4),
(112, '2022-01-04', 'Altria', 'TJ&S', 4),
(113, '2022-01-05', 'Altria', 'TJ&S', 4),
(114, '2022-01-07', 'Altria', 'TJ&S', 4),
(115, '2022-01-06', 'Altria', 'TJ&S', 4),
(116, '2022-01-10', 'Altria', 'TJ&S', 3),
(117, '2022-01-11', 'Altria', 'TJ&S', 3),
(118, '2022-01-12', 'Altria', 'TJ&S', 4),
(119, '2022-01-14', 'Altria', 'TJ&S', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ap_tbl`
--
ALTER TABLE `ap_tbl`
  ADD PRIMARY KEY (`ap_id`);

--
-- Indexes for table `bh_tbl`
--
ALTER TABLE `bh_tbl`
  ADD PRIMARY KEY (`bh_id`);

--
-- Indexes for table `hl_tbl`
--
ALTER TABLE `hl_tbl`
  ADD PRIMARY KEY (`hl_id`);

--
-- Indexes for table `lc_tbl`
--
ALTER TABLE `lc_tbl`
  ADD PRIMARY KEY (`lc_id`);

--
-- Indexes for table `pt_tbl`
--
ALTER TABLE `pt_tbl`
  ADD PRIMARY KEY (`pt_id`);

--
-- Indexes for table `qi_tbl`
--
ALTER TABLE `qi_tbl`
  ADD PRIMARY KEY (`qi_id`);

--
-- Indexes for table `sgf_tbl`
--
ALTER TABLE `sgf_tbl`
  ADD PRIMARY KEY (`sgf_id`);

--
-- Indexes for table `sg_tbl`
--
ALTER TABLE `sg_tbl`
  ADD PRIMARY KEY (`sg_id`);

--
-- Indexes for table `si_tbl`
--
ALTER TABLE `si_tbl`
  ADD PRIMARY KEY (`si_id`);

--
-- Indexes for table `ts_tbl`
--
ALTER TABLE `ts_tbl`
  ADD PRIMARY KEY (`ts_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ap_tbl`
--
ALTER TABLE `ap_tbl`
  MODIFY `ap_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bh_tbl`
--
ALTER TABLE `bh_tbl`
  MODIFY `bh_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `hl_tbl`
--
ALTER TABLE `hl_tbl`
  MODIFY `hl_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `lc_tbl`
--
ALTER TABLE `lc_tbl`
  MODIFY `lc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=159;

--
-- AUTO_INCREMENT for table `pt_tbl`
--
ALTER TABLE `pt_tbl`
  MODIFY `pt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `qi_tbl`
--
ALTER TABLE `qi_tbl`
  MODIFY `qi_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sgf_tbl`
--
ALTER TABLE `sgf_tbl`
  MODIFY `sgf_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sg_tbl`
--
ALTER TABLE `sg_tbl`
  MODIFY `sg_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `si_tbl`
--
ALTER TABLE `si_tbl`
  MODIFY `si_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ts_tbl`
--
ALTER TABLE `ts_tbl`
  MODIFY `ts_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=120;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
