
<?php
       
    include('header.php');

    session_start();

    if(isset($_SESSION['username']))
    {
       $_SESSION['username'];
    }
    else
    {
        header("location:../../index.php");
    }


?> 

  <body class="bg-light">
    
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="#"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-file-minus" viewBox="0 0 16 16">
  <path d="M5.5 8a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H6a.5.5 0 0 1-.5-.5z"/>
  <path d="M4 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H4zm0 1h8a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1z"/>
</svg>&nbsp;Bluvys</a>

  </div>
</nav>

<main class="container"><br><br><br>
  <div class="bg-light p-5 rounded">
    <div class="card bg-light border-0">
      <div class="row">
          <div class="col-lg-4">
          </div>
          <div class="col-lg-4">
            <div class="card-body bg-white border-1 shadow-lg">
              <h3 class="m-1 p-1 text-center">Add Report</h3>
                        <script>
                                    function validateForm() {
                                      var x = document.forms["myForm"]["account_id"].value;    
                                      var x = document.forms["myForm"]["call_date"].value;
                                      var x = document.forms["myForm"]["actual_agent_talktime"].value;
                                      var x = document.forms["myForm"]["answered_calls"].value;
                                      var x = document.forms["myForm"]["dropped_calls"].value;
                                      var x = document.forms["myForm"]["hold_calls"].value;
                                      var x = document.forms["myForm"]["total_holdtime"].value;


                                      if (x == "" || x == null) {
                                        alert("Fill all blanks!!!");
                                        return false;
                                      }
                                    }





                                    function mult(value){


                                        var acis, act, tct, tctis, tic;
                                        var aht , thtis, hp, ahtis , ad, dp, aqa, acp, taqa;

                                        var agtt = document.getElementById('agtt').value;
                                        var ac = document.getElementById('ac').value;
                                        var tht = document.getElementById('tht').value;
                                        var hc = document.getElementById('hc').value;
                                        var dc = document.getElementById('dc').value;


                                          

                                          act = agtt / ac;
                                          acis = act * 60;
                                          tct = agtt;
                                          tctis = tct * 60; 
                                          tic = ac;     

                                          if (hc == 0 || hc == null) {

                                            aht = 0;
                                          }
                                          else{
                                            aht = tht / hc;
                                          }
                                      


                                          thtis = tht * 60;
                                          hp = hc / ac * 100;
                                          ahtis = aht * 60;
                                          ad = dc / ac;
                                          dp = dc / ac * 100;
                                          acp = 100 - dp;
                                            

                                          aqa = Math.floor(Math.random() * 60);

                                          taqa = aqa / 60;



                                        var actcon = act.toFixed(2);
                                        var aciscon = acis.toFixed(2);
                                        var tctiscon = tctis.toFixed(2);
                                        var ahtcon = aht.toFixed(2);
                                        var thtiscon = thtis.toFixed(2);
                                        var hpcon = hp.toFixed(2);
                                        var ahtiscon = ahtis.toFixed(2);
                                        var adcon = ad.toFixed(2);
                                        var dpcon = dp.toFixed(2);
                                        var acpcon = acp.toFixed(2);
                                        var taqacon = taqa.toFixed(2);


                                        document.getElementById('acis').value = aciscon;
                                        document.getElementById('act').value = actcon;
                                        document.getElementById('tct').value = tct;
                                        document.getElementById('tctis').value = tctiscon;
                                        document.getElementById('tic').value = tic;                  
                                        document.getElementById('aht').value = ahtcon;
                                        document.getElementById('thtis').value = thtiscon;
                                        document.getElementById('hp').value = hpcon;
                                        document.getElementById('ahtis').value = ahtiscon;
                                        document.getElementById('ad').value = adcon;
                                        document.getElementById('dp').value = dpcon;
                                        document.getElementById('acp').value = acpcon;
                                        document.getElementById('aqa').value = taqacon;
                                        }


                                       


                                    </script>


                <form  name="myForm" onsubmit="return validateForm()" required>
                  <div class="form-floating mb-2">
                    <input class="form-control" placeholder="Account ID" name="account_id" value="10067" id="account_id">
                    <label for="account_id">Account ID</label>
                  </div>
                  <div class="form-floating mb-2">
                    <input type="hidden" class="form-control" placeholder="Username" name="username" value="altria" id="username">
                  </div>
                  <div class="form-floating mb-2">
                    <input type="hidden" class="form-control" placeholder="Password" name="password" value="altria652pj%" id="passsword">
                  </div>
                  <div class="form-floating mb-2">
                    <input type="date" class="form-control" placeholder="Call Date" name="call_date" placeholder="yyyy-mm-dd" min="2019-01-01" max="2030-12-31" id="call_date">
                    <label for="call_date">Date</label>
                  </div>
                  <div class="form-floating mb-2">
                    <input class="form-control" placeholder="Actual Agent Talktime" name="actual_agent_talktime" id="agtt" onkeyup="mult(this.value);" >
                    <label for="agtt">Actual Agent Talktime</label>
                  </div>
                  <div class="form-floating mb-2">
                    <input type="hidden"  class="form-control" placeholder="After Hours Calls" name="after_hours_calls" value="0" id="ahc">
                  </div>
                  <div class="form-floating mb-2">
                    <input class="form-control" placeholder="Answered Calls" name="answered_calls" id="ac" onkeyup="mult(this.value);">
                    <label for="ac">Answered Calls</label>
                  </div>
                  <div class="form-floating mb-2">
                    <input type="hidden" class="form-control" placeholder="Answered Calls Percent" name="answered_calls_percent" id="acp">
                  </div>
                  <div class="form-floating mb-2">
                    <input type="hidden" class="form-control" placeholder="Average Calltime in Sec" name="average_calltime_in_sec" id="acis">
                  </div>
                  <div class="form-floating mb-2">
                    <input type="hidden" class="form-control" placeholder="Average Calltime" name="average_calltime" id="act">
                  </div>
                  <div class="form-floating mb-2">
                    <input type="hidden" class="form-control" placeholder="Average Drop" name="average_drop" id="ad">
                  </div>
                  <div class="form-floating mb-2">
                    <input  type="hidden" class="form-control" placeholder="Average Hold Time" name="average_hold_time" id="aht">
                  </div>
                  <div class="form-floating mb-2">
                    <input  type="hidden" class="form-control" placeholder="Average Holdtime in Sec" name="average_holdtime_in_sec" id="ahtis">
                  </div>
                  <div class="form-floating mb-2">
                    <input  type="hidden" class="form-control" placeholder="Average Queue Answered" name="average_queue_answered" id="aqa">
                  </div>
                  <div class="form-floating mb-2">
                    <input type="hidden" class="form-control" placeholder="Drop Percent" name="drop_percent" id="dp">
                  </div>
                  <div class="form-floating mb-2">
                    <input  class="form-control" placeholder="Dropped Calls" name="dropped_calls" id="dc" onkeyup="mult(this.value);">
                    <label for="dc">Dropped Calls</label>
                  </div>
                  <div class="form-floating mb-2">
                    <input type="hidden" class="form-control" placeholder="Hold 30 Sec" name="hold_30sec" value="0" id="h3s">
                  </div>
                  <div class="form-floating mb-2">
                    <input  type="hidden" class="form-control" placeholder="Hold 30 Sec Percent" name="hold_30sec_percent" value="0" id="h3sp">
                  </div>
                  <div class="form-floating mb-2">
                    <input type="hidden" class="form-control" placeholder="Hold 60 Sec" name="hold_60sec" value="0" id="h6s">
                  </div>
                  <div class="form-floating mb-2">
                    <input type="hidden" class="form-control" placeholder="Hold 60 Sec Percent" name="hold_60sec_percent" value="0" id="h6sp">
                  </div>
                  <div class="form-floating mb-2">
                    <input class="form-control" placeholder="Hold Calls" name="hold_calls" id="hc" onkeyup="mult(this.value);">
                    <label for="hc">Hold Calls</label>
                  </div>
                  <div class="form-floating mb-2">
                    <input type="hidden" class="form-control" placeholder="Hold Percent" name="hold_percent" id="hp">
                  </div>
                  <div class="form-floating mb-2">
                    <input type="hidden" class="form-control" placeholder="Title" name="title" value="Bluvys" id="title">
                  </div>
                  <div class="form-floating mb-2">
                    <input type="hidden" class="form-control" placeholder="Total Calltime" name="total_calltime" id="tct">
                  </div>
                  <div class="form-floating mb-2">
                    <input type="hidden" class="form-control" placeholder="Total Calltime in Sec" name="total_calltime_in_sec" id="tctis">
                  </div>
                  <div class="form-floating mb-2">
                    <input class="form-control" placeholder="Total Holdtime" name="total_holdtime" id="tht" onkeyup="mult(this.value);">
                    <label for="tht">Total Holdtime</label>
                  </div>
                  <div class="form-floating mb-2">
                    <input  type="hidden" class="form-control" placeholder="Total Holdtime in Sec" name="total_holdtime_in_sec" id="thtis">
                  </div>
                  <div class="form-floating mb-2">
                    <input  type="hidden"class="form-control" placeholder="Total Inbound Calls" name="total_inbound_calls" id="tic">
                  </div>
                  <div class="form-floating mb-2">
                    <input  type="hidden"class="form-control" placeholder="CC Name" name="cc_name" value="Bluvys" id="cc_name">
                  </div>
                  <div class="form-floating mb-2">
                    <input type="hidden" class="form-control" placeholder="Status Refund" name="status_refund" value="0" id="sr">
                  </div>
                  <div class="form-floating mb-2">
                    <input type="hidden"  class="form-control" placeholder="Status Cancellation" name="status_cancellation" value="0" id="sc">
                  </div>
                  <div class="form-floating mb-2">
                    <input type="hidden" class="form-control" placeholder="Status Sales" name="status_sales" value="0" id="ss">
                  </div>
                  <div class="form-floating mb-2">
                    <input type="hidden" class="form-control" placeholder="Status Sales Saved" name="status_sales_saved" value="0" id="sss">
                  </div>
                  <div class="form-floating mb-2">
                    <input type="hidden" class="form-control" placeholder="Transferred Calls" name="transferred_calls" value="0" id="tc">
                  </div>
                  <div class="form-floating mb-2">
                    <input type="hidden" class="form-control" placeholder="Agent Id" name="agent_id" value="Altria" id="ag">
                  </div>
                  <div class="form-floating mb-2">
                   <div class="row">
                    <div class="col-lg-4">
                      <button class="w-100 btn btn-md btn-primary" onclick="myForm.method='get'; myForm.action='https://rapidphonecenter.com/admin/sapi/inbound_api_account.php?'; this.form.target='_blank';return true;">API 1</button>
                    </div>
                    <div class="col-lg-4">
                      <button class="w-100 btn btn-md btn-primary" onclick="myForm.method='get'; myForm.action='https://rapidphonecenter.com/admin/sapi/inbound_api.php?'; this.form.target='_blank';return true;">API 2</button>
                    </div>
                    <div class="col-lg-4">
                      <button class="w-100 btn btn-md btn-primary" onclick="myForm.method='post'; myForm.action='../../action/inb_data.php'; this.form.target='_blank';return true;" name="save">Internal</button>  
                    </div>
                   </div>
                  </div>                                             
              </form>


            </div>
          </div>
          <div class="col-lg-4">
        
          </div>
      </div>
    </div>
  </div>
</main>


    <script src="../../bootstrap/js/bootstrap.bundle.min.js"></script>

      
  </body>
</html>
