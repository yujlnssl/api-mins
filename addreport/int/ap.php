
<?php
       
    include('header.php');

    session_start();

    if(isset($_SESSION['username']))
    {
       $_SESSION['username'];
    }
    else
    {
        header("location:../../index.php");
    }


?> 

  <body class="bg-light">
    
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="#"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-file-minus" viewBox="0 0 16 16">
  <path d="M5.5 8a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H6a.5.5 0 0 1-.5-.5z"/>
  <path d="M4 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H4zm0 1h8a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1z"/>
</svg>&nbsp;Affiliate Payments</a>

  </div>
</nav>

<main class="container"><br><br><br><br>
  <div class="bg-light p-5 rounded">
    <div class="card bg-light border-0">
      <div class="row">
          <div class="col-lg-4">
          </div>
          <div class="col-lg-4">
            <div class="card-body bg-white border-1 shadow-lg">
              <h3 class="m-1 p-1 text-center">Add Report</h3>
                                    <script>
                                           function validateForm() {
                                            var x = document.forms["myForm"]["call_date"].value;
                                            var x = document.forms["myForm"]["agent_name"].value;
                                            var x = document.forms["myForm"]["account_name"].value;
                                            var x = document.forms["myForm"]["hours"].value;

                                            if (x == "" || x == null) {
                                              alert("Fill all blanks!!!");
                                              return false;
                                            }
                                          }


                                          function mult(value){


                                              var total;
                                              var hours = document.getElementById('hours').value;

                                              total = 60 * hours;


                                              document.getElementById('mins').value = total;


                                          }
                                    </script>


                <form  name="myForm" onsubmit="return validateForm()" required>
                   <div class="form-floating mb-2">
                    <input type="date" class="form-control" placeholder="Call Date" name="call_date" placeholder="yyyy-mm-dd" min="2019-01-01" max="2030-12-31" id="call_date">
                    <label for="call_date">Date</label>
                  </div>
                  <div class="form-floating mb-2">
                   <input class="form-control" placeholder="Agent Name" name="agent_name" id="agent_name">
                   <label for="agent_name">Agent Name</label>
                  </div>
                  <div class="form-floating mb-2">
                   <input type="hidden" class="form-control" placeholder="Account Name" name="account_name" value="Affiliate Payments" id="account_name">
                  </div>
                  <div class="form-floating mb-2">
                   <input class="form-control" placeholder="Hours" name="hours" onkeyup="mult(this.value);" id="hours">
                   <label for="hours">Hours</label>
                  </div>
                  <div class="form-floating mb-2">
                      <button class="w-100 btn btn-md btn-primary" name="save2" onclick="myForm.method='post'; myForm.action='../../action/int_data.php'; this.form.target='_blank';return true;">Internal</button>
                  </div>                                             
              </form>
            </div>
          </div>
          <div class="col-lg-4">
        
          </div>
      </div>
    </div>
  </div>
</main>


    <script src="../../bootstrap/js/bootstrap.bundle.min.js"></script>

      
  </body>
</html>
