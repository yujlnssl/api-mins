<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Address Verification Summary</title>
    

    <!-- Bootstrap core CSS -->
<link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>

    
  </head>
  <body class="bg-light">
    
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="#"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-bookmark" viewBox="0 0 16 16">
  <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v13.5a.5.5 0 0 1-.777.416L8 13.101l-5.223 2.815A.5.5 0 0 1 2 15.5V2zm2-1a1 1 0 0 0-1 1v12.566l4.723-2.482a.5.5 0 0 1 .554 0L13 14.566V2a1 1 0 0 0-1-1H4z"/>
</svg>&nbsp;Address Verification Summary Report</a>

  </div>
</nav>

<main class="container"><br><br>
  <div class="bg-light p-5 rounded">

    <div class="header container">
            <div class="row">
                 <div class="col-lg-5">
                  <form action="" method="GET" class="" >
                    <div class="card shadow-lg border border-1">
                        <div class="m-2 p-2">
                            <div class="form-group">
                              <div class="row">
                                  <div class="col-md-5"> 
                                    <label for="from_date" class="p-2">Start Date</label>
                                   </div>
                                     <div class="col-md-7"> 
                                      <input type="date" name="from_date" value="<?php if(isset($_GET['from_date'])){echo $_GET['from_date'];} else{}  ?>" class="form-control" placeholder="From Date">
                                   </div>
                                  </div>
                              </div>
                              <div class="form-group">
                                <div class="row">
                                  <div class="col-md-5"> 
                                    <label for="to_date" class="p-2">End Date</label>
                                  </div>
                                   <div class="col-md-7"> 
                                  <input type="date" name="to_date" value="<?php if(isset($_GET['to_date'])){echo $_GET['to_date'];} else{}  ?>" class="form-control" placeholder="From Date">
                                  </div>
                                </div>
                              </div>  
                              <hr> 
                               <div class="form-group"> 
                                 <div class="row">
                                  <div class="col-md-8"> 
                                    </div>
                                     <div class="col-md-4"> 
                                        <button type="submit" class="w-100 btn btn-outline-secondary bg-gradient" id="save">Search</button>
                                    </div>      
                                </div>
                               </div>                      
                            
                        </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-7">
                    
                </div>
            </div>
            <br><br>
      <div class="card shadow-lg border border-1">
        <div class="card-body ">
              <table class="table table-bordered table-hover" style="font-size: 13px;">
                                         <thead>
                                            <tr>
                                               

                                            </tr>
                                          </thead>
                                      <tbody>

                                          <tr>
                                                
                                            <td class="text-dark bg-light"><b>CAMPAIGN NAME</b></td>                        
                                            <td class="text-dark text-center bg-light"><b>HOURS</b></td>
                                          </tr>

                                            <?php
                                            $con = mysqli_connect("localhost", "root", "Altria123!@#", "adb3");
                                                                                    
                                            if(isset($_GET['from_date']) && isset($_GET['to_date'])){


                                              if(strtotime($_GET['from_date']) <= strtotime($_GET['to_date'])){

                                              $from_date = $_GET['from_date'];
                                              $to_date = $_GET['to_date'];

                                              
                                                
                                              $sql1 = "SELECT SUM(hours) as total1 FROM qi_tbl WHERE call_date BETWEEN '$from_date' AND '$to_date'";
                                              $sql2 = "SELECT SUM(hours) as total2 FROM ts_tbl WHERE call_date BETWEEN '$from_date' AND '$to_date'";
                                              $sql3 = "SELECT SUM(hours) as total3 FROM sg_tbl WHERE call_date BETWEEN '$from_date' AND '$to_date'";
                                              $sql4 = "SELECT SUM(hours) as total4 FROM sgf_tbl WHERE call_date BETWEEN '$from_date' AND '$to_date'";
                                              $sql5 = "SELECT SUM(hours) as total5 FROM hl_tbl WHERE call_date BETWEEN '$from_date' AND '$to_date'";
                                              $sql6 = "SELECT SUM(hours) as total6 FROM si_tbl WHERE call_date BETWEEN '$from_date' AND '$to_date'";
                                              $sql7 = "SELECT SUM(hours) as total7 FROM pt_tbl WHERE call_date BETWEEN '$from_date' AND '$to_date'";
                                              $sql8 = "SELECT SUM(hours) as total8 FROM bh_tbl WHERE call_date BETWEEN '$from_date' AND '$to_date'";
                                              $sql9 = "SELECT SUM(hours) as total9 FROM ap_tbl WHERE call_date BETWEEN '$from_date' AND '$to_date'";
                                             


                                              
                                              

                                              $sql16 = "SELECT (SELECT COALESCE(SUM(hours), 0) FROM qi_tbl WHERE call_date BETWEEN '$from_date' AND '$to_date') + (SELECT COALESCE(SUM(hours), 0) FROM ts_tbl WHERE call_date BETWEEN '$from_date' AND '$to_date') + (SELECT COALESCE(SUM(hours), 0) FROM sg_tbl WHERE call_date BETWEEN '$from_date' AND '$to_date') + (SELECT COALESCE(SUM(hours), 0) FROM sgf_tbl WHERE call_date BETWEEN '$from_date' AND '$to_date') + (SELECT COALESCE(SUM(hours), 0) FROM hl_tbl WHERE call_date BETWEEN '$from_date' AND '$to_date') + (SELECT COALESCE(SUM(hours), 0) FROM si_tbl WHERE call_date BETWEEN '$from_date' AND '$to_date') + (SELECT COALESCE(SUM(hours), 0) FROM pt_tbl WHERE call_date BETWEEN '$from_date' AND '$to_date') + (SELECT COALESCE(SUM(hours), 0) FROM bh_tbl WHERE call_date BETWEEN '$from_date' AND '$to_date') + (SELECT COALESCE(SUM(hours), 0) FROM ap_tbl WHERE call_date BETWEEN '$from_date' AND '$to_date') as total16";




                                              $sql_result1 = mysqli_query($con, $sql1) or die (mysqli_error($con));
                                              $sql_result2 = mysqli_query($con, $sql2) or die (mysqli_error($con));
                                              $sql_result3 = mysqli_query($con, $sql3) or die (mysqli_error($con));
                                              $sql_result4 = mysqli_query($con, $sql4) or die (mysqli_error($con));
                                              $sql_result5 = mysqli_query($con, $sql5) or die (mysqli_error($con));
                                              $sql_result6 = mysqli_query($con, $sql6) or die (mysqli_error($con));
                                              $sql_result7 = mysqli_query($con, $sql7) or die (mysqli_error($con));
                                              $sql_result8 = mysqli_query($con, $sql8) or die (mysqli_error($con));
                                              $sql_result9 = mysqli_query($con, $sql9) or die (mysqli_error($con));
                                              $sql_result16 = mysqli_query($con, $sql16) or die (mysqli_error($con));






                                              if (mysqli_num_rows($sql_result1) > 0 ) {

                                                
                                                while ($row = mysqli_fetch_assoc($sql_result1)) {
                                                  ?>
                                                  <tr>

                                                    <td class="text-dark" >QUIC Industries</td>                        
                                                    <td class="text-dark text-center"><?php echo $row['total1'];?></td>
                                                  </tr>

                                                  <?php
                                                }
                                              }
                                              if (mysqli_num_rows($sql_result2) > 0 ) {

                                                
                                                while ($row = mysqli_fetch_assoc($sql_result2)) {
                                                  ?>
                                                  <tr>
                                                    <td class="text-dark" >TJ&S</td>                        
                                                    <td class="text-dark text-center"><?php echo $row['total2'];?></td>
                                                  </tr>

                                                  <?php
                                                }
                                              }
                                              if (mysqli_num_rows($sql_result3) > 0 ) {

                                                
                                                while ($row = mysqli_fetch_assoc($sql_result3)) {
                                                  ?>
                                                  <tr>
                                                    <td class="text-dark" >Shen Group</td>                       
                                                    <td class="text-dark text-center"><?php echo $row['total3'];?></td>
                                                  </tr>

                                                  <?php
                                                }
                                              }
                                              if (mysqli_num_rows($sql_result4) > 0 ) {

                                                
                                                while ($row = mysqli_fetch_assoc($sql_result4)) {
                                                  ?>
                                                  <tr>
                                                    <td class="text-dark" >Shen Group FL</td>                       
                                                    <td class="text-dark text-center"><?php echo $row['total4'];?></td>
                                                  </tr>

                                                  <?php
                                                }
                                              }
                                              if (mysqli_num_rows($sql_result5) > 0 ) {

                                                
                                                while ($row = mysqli_fetch_assoc($sql_result5)) {
                                                  ?>
                                                  <tr>
                                                    <td class="text-dark" >Healthy Living</td>                       
                                                    <td class="text-dark text-center"><?php echo $row['total5'];?></td>
                                                  </tr>

                                                  <?php
                                                }
                                              }
                                              if (mysqli_num_rows($sql_result6) > 0 ) {

                                                
                                                while ($row = mysqli_fetch_assoc($sql_result6)) {
                                                  ?>
                                                  <tr>
                                                    <td class="text-dark" >Squid Ink</td>                       
                                                    <td class="text-dark text-center"><?php echo $row['total6'];?></td>
                                                  </tr>

                                                  <?php
                                                }
                                              }
                                              if (mysqli_num_rows($sql_result7) > 0 ) {

                                                
                                                while ($row = mysqli_fetch_assoc($sql_result7)) {
                                                  ?>
                                                  <tr>
                                                    <td class="text-dark" >Patriots</td>                       
                                                    <td class="text-dark text-center"><?php echo $row['total7'];?></td>
                                                  </tr>

                                                  <?php
                                                }
                                              }
                                              if (mysqli_num_rows($sql_result8) > 0 ) {

                                                
                                                while ($row = mysqli_fetch_assoc($sql_result8)) {
                                                  ?>
                                                  <tr>
                                                    <td class="text-dark" >Beauty Health</td>                       
                                                    <td class="text-dark text-center"><?php echo $row['total8'];?></td>
                                                  </tr>

                                                  <?php
                                                }
                                              }
                                              if (mysqli_num_rows($sql_result9) > 0 ) {

                                                
                                                while ($row = mysqli_fetch_assoc($sql_result9)) {
                                                  ?>
                                                  <tr>
                                                    <td class="text-dark" >Affiliate Payments</td>                       
                                                    <td class="text-dark text-center"><?php echo $row['total9'];?></td>
                                                  </tr>

                                                  <?php
                                                }
                                              }



                                                
                                              if (mysqli_num_rows($sql_result16) > 0 ) {

                                                
                                                while ($row = mysqli_fetch_assoc($sql_result16)) {
                                                  ?>
                                                  <tr>
                                                    <td class="text-dark bg-light" ><b>TOTAL HOURS</b></td>                        
                                                    <td class="text-dark text-center bg-light"><b><?php echo $row['total16'];?></b></td>
                                                  </tr>

                                                  <?php
                                                }
                                              }
                                            

                                              
                                                                                                                            

                                            }
                                            else{
                                              ?>
                                                <tr>
                                                  <td colspan="2"><h5 class="text-dark"><?php echo "NO RESULT TOTAL HOURS"; ?></h5></td>
                                                </tr>
                                              <?php
                                            }
                  } 

                                          ?>

                                  </tbody>
                                </table>
        </div>
      </div>

  </div>
</main>


    <script src="../bootstrap/js/bootstrap.bundle.min.js"></script>

      
  </body>
</html>
