<?php
    session_start();

    if(isset($_SESSION['username']))
    {
       $_SESSION['username'];
    }
    else
    {
        header("location:../index.php");
    }


?> 

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Dashboard</title>
<link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link rel="shortcut icon" href="../favicon.ico" />

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
      .scroll{
      	height: 350px;
      	overflow: scroll;
      }
    </style>

    
    <!-- Custom styles for this template -->
    <link href="../style/sidebars.css" rel="stylesheet">
  </head>
  <body>
    
<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
  <symbol id="api" viewBox="0 0 16 16">
      <path d="M8.5 6a.5.5 0 0 0-1 0v1.5H6a.5.5 0 0 0 0 1h1.5V10a.5.5 0 0 0 1 0V8.5H10a.5.5 0 0 0 0-1H8.5V6z"/>
  <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2zm10-1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1z"/>
  </symbol>
    <symbol id="dashboard" viewBox="0 0 16 16">
       <path d="M8 4a.5.5 0 0 1 .5.5V6a.5.5 0 0 1-1 0V4.5A.5.5 0 0 1 8 4zM3.732 5.732a.5.5 0 0 1 .707 0l.915.914a.5.5 0 1 1-.708.708l-.914-.915a.5.5 0 0 1 0-.707zM2 10a.5.5 0 0 1 .5-.5h1.586a.5.5 0 0 1 0 1H2.5A.5.5 0 0 1 2 10zm9.5 0a.5.5 0 0 1 .5-.5h1.5a.5.5 0 0 1 0 1H12a.5.5 0 0 1-.5-.5zm.754-4.246a.389.389 0 0 0-.527-.02L7.547 9.31a.91.91 0 1 0 1.302 1.258l3.434-4.297a.389.389 0 0 0-.029-.518z"/>
      <path fill-rule="evenodd" d="M0 10a8 8 0 1 1 15.547 2.661c-.442 1.253-1.845 1.602-2.932 1.25C11.309 13.488 9.475 13 8 13c-1.474 0-3.31.488-4.615.911-1.087.352-2.49.003-2.932-1.25A7.988 7.988 0 0 1 0 10zm8-7a7 7 0 0 0-6.603 9.329c.203.575.923.876 1.68.63C4.397 12.533 6.358 12 8 12s3.604.532 4.923.96c.757.245 1.477-.056 1.68-.631A7 7 0 0 0 8 3z"/>
    </symbol>  
    <symbol id="logout" viewBox="0 0 16 16">
      <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>
     <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>
   </symbol>
   <symbol id="report" viewBox="0 0 16 16">
     <path d="M10.854 7.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 9.793l2.646-2.647a.5.5 0 0 1 .708 0z"/>
     <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z"/>

  </symbol>

  <symbol id="campaign" viewBox="0 0 16 16">
   <path d="M10.854 6.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 8.793l2.646-2.647a.5.5 0 0 1 .708 0z"/>
   <path d="M4 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H4zm0 1h8a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1z"/>
  </symbol> 

   <symbol id="camp-report" viewBox="0 0 16 16"> 
     <path d="M5 4a.5.5 0 0 0 0 1h6a.5.5 0 0 0 0-1H5zm-.5 2.5A.5.5 0 0 1 5 6h6a.5.5 0 0 1 0 1H5a.5.5 0 0 1-.5-.5zM5 8a.5.5 0 0 0 0 1h6a.5.5 0 0 0 0-1H5zm0 2a.5.5 0 0 0 0 1h3a.5.5 0 0 0 0-1H5z"/>
      <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2zm10-1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1z"/>
   </symbol>   


</svg>

<main>
  <div class="d-flex flex-column flex-shrink-0 p-3 text-dark bg-light" style="width: 230px;">
    <br>
      <a href="image.php" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-dark text-decoration-none" target="main_frame">
      <img src="pro.png" class="img-thumbnail rounded" alt="profile" style="max-width:: 90px; max-height: 90px;">
      <span class="fs-4 ps-2">Vanessa</span>
    </a>
    <br>
    <hr>
    <ul class="list-unstyled ps-0">
      <li class="mb-1">
        <button class="btn btn-toggle align-items-center collapsed rounded text-dark" data-bs-toggle="collapse" data-bs-target="#dashboard-collapse" aria-expanded="false">
                Dashboard
              </button>
              <div class="collapse" id="dashboard-collapse">
                <ul class="btn-toggle-nav list-unstyled fw-normal pb-1 small">
                  <li><a href="#" class="link-dark rounded text-dark" data-bs-toggle="collapse" data-bs-target="#dashboardx-collapse" aria-expanded="false">
                  <svg class="bi me-3" width="16" height="16"><use xlink:href="#report"/></svg>Inbound Report</a>

                  <div class="collapse ps-2 scroll" id="dashboardx-collapse">
                    <ul class="btn-toggle-nav list-unstyled fw-normal pb-1 small">
                      <li><a href="../addreport/inb/dbl.php" class="link-dark rounded text-dark" target="main_frame">
                      <svg class="bi me-3" width="16" height="16"><use xlink:href="#camp-report"/></svg>Dignity Bio Lab</a></li>
                      <li><a href="../addreport/inb/sbb.php" class="link-dark rounded text-dark" target="main_frame">
                      <svg class="bi me-3" width="16" height="16"><use xlink:href="#camp-report"/></svg>Saybyebugs</a></li>
                      <li><a href="../addreport/inb/stb.php" class="link-dark rounded text-dark" target="main_frame">
                      <svg class="bi me-3" width="16" height="16"><use xlink:href="#camp-report"/></svg>Signal Tech Booster</a></li>
                      <li><a href="../addreport/inb/sa.php" class="link-dark rounded text-dark" target="main_frame">
                      <svg class="bi me-3" width="16" height="16"><use xlink:href="#camp-report"/></svg>Safe Alarm</a></li>
                      <li><a href="../addreport/inb/aj.php" class="link-dark rounded text-dark" target="main_frame">
                      <svg class="bi me-3" width="16" height="16"><use xlink:href="#camp-report"/></svg>Airjoi</a></li>
                      <li><a href="../addreport/inb/fn.php" class="link-dark rounded text-dark" target="main_frame">
                      <svg class="bi me-3" width="16" height="16"><use xlink:href="#camp-report"/></svg>Fitnus</a></li>
                      <li><a href="../addreport/inb/fnb.php" class="link-dark rounded text-dark" target="main_frame">
                      <svg class="bi me-3" width="16" height="16"><use xlink:href="#camp-report"/></svg>Fitnus Brace</a></li>
                      <li><a href="../addreport/inb/gb.php" class="link-dark rounded text-dark" target="main_frame">
                      <svg class="bi me-3" width="16" height="16"><use xlink:href="#camp-report"/></svg>Glow Birds</a></li>
                      <li><a href="../addreport/inb/bl.php" class="link-dark rounded text-dark" target="main_frame">
                      <svg class="bi me-3" width="16" height="16"><use xlink:href="#camp-report"/></svg>Bluvys</a></li>
                      <li><a href="../addreport/inb/dm.php" class="link-dark rounded text-dark" target="main_frame">
                      <svg class="bi me-3" width="16" height="16"><use xlink:href="#camp-report"/></svg>Direct Media</a></li>
                      <li style="display:none;"><a href="../addreport/inb/az.php" class="link-dark rounded text-dark" target="main_frame">
                      <svg class="bi me-3" width="16" height="16"><use xlink:href="#camp-report"/></svg>Azurro</a></li>
                      <li><a href="../addreport/inb/is.php" class="link-dark rounded text-dark" target="main_frame">
                      <svg class="bi me-3" width="16" height="16"><use xlink:href="#camp-report"/></svg>Identity Shield</a></li>
                      <li><a href="../addreport/inb/dc.php" class="link-dark rounded text-dark" target="main_frame">
                      <svg class="bi me-3" width="16" height="16"><use xlink:href="#camp-report"/></svg>Dash Cam Pro</a></li>
                      <li><a href="../addreport/inb/tl.php" class="link-dark rounded text-dark" target="main_frame">
                      <svg class="bi me-3" width="16" height="16"><use xlink:href="#camp-report"/></svg>Tai Lopez</a></li>
                      <li><a href="../addreport/inb/mb.php" class="link-dark rounded text-dark" target="main_frame">
                      <svg class="bi me-3" width="16" height="16"><use xlink:href="#camp-report"/></svg>MentorBox</a></li>
                      <li><a href="../addreport/inb/hm.php" class="link-dark rounded text-dark" target="main_frame">
                      <svg class="bi me-3" width="16" height="16"><use xlink:href="#camp-report"/></svg>Hotel Motel</a></li>
                      <li><a href="../addreport/inb/as.php" class="link-dark rounded text-dark" target="main_frame">
                      <svg class="bi me-3" width="16" height="16"><use xlink:href="#camp-report"/></svg>Au Sante</a></li>
                      <li><a href="../addreport/inb/sl.php" class="link-dark rounded text-dark" target="main_frame">
                      <svg class="bi me-3" width="16" height="16"><use xlink:href="#camp-report"/></svg>Skyline</a></li>
                    </ul>
                 </div>

                </li>
                  <li><a href="#" class="link-dark rounded text-dark" data-bs-toggle="collapse" data-bs-target="#dashboardx2-collapse" aria-expanded="false">
                  <svg class="bi me-3" width="16" height="16"><use xlink:href="#report"/></svg>Rev Brand Report</a>
                      <div class="collapse ps-2" id="dashboardx2-collapse">
                        <ul class="btn-toggle-nav list-unstyled fw-normal pb-1 small">
                          <li><a href="../addreport/rev/fc.php" class="link-dark rounded text-dark" target="main_frame">
                          <svg class="bi me-3" width="16" height="16"><use xlink:href="#camp-report"/></svg>Farmerscart</a></li>
                          <li><a href="../addreport/rev/fm.php" class="link-dark rounded text-dark" target="main_frame">
                          <svg class="bi me-3" width="16" height="16"><use xlink:href="#camp-report"/></svg>Franklin Mint</a></li>
                          <li><a href="../addreport/rev/rs.php" class="link-dark rounded text-dark" target="main_frame">
                          <svg class="bi me-3" width="16" height="16"><use xlink:href="#camp-report"/></svg>Radioshack</a></li>
                        </ul>
                     </div>  
                    </li>

                    <li><a href="#" class="link-dark rounded text-dark" data-bs-toggle="collapse" data-bs-target="#dashboardx3-collapse" aria-expanded="false">
                  <svg class="bi me-3" width="16" height="16"><use xlink:href="#report"/></svg>Address Verification</a>
                      <div class="collapse ps-2" id="dashboardx3-collapse">
                        <ul class="btn-toggle-nav list-unstyled fw-normal pb-1 small">
                          <li><a href="../addreport/int/qi.php" class="link-dark rounded text-dark" target="main_frame">
                          <svg class="bi me-3" width="16" height="16"><use xlink:href="#camp-report"/></svg>QUIC Industries</a></li>
                          <li><a href="../addreport/int/ts.php" class="link-dark rounded text-dark" target="main_frame">
                          <svg class="bi me-3" width="16" height="16"><use xlink:href="#camp-report"/></svg>TJ&S</a></li>
                          <li><a href="../addreport/int/sg.php" class="link-dark rounded text-dark" target="main_frame">
                          <svg class="bi me-3" width="16" height="16"><use xlink:href="#camp-report"/></svg>Shen Group</a></li>
                          <li><a href="../addreport/int/sgf.php" class="link-dark rounded text-dark" target="main_frame">
                          <svg class="bi me-3" width="16" height="16"><use xlink:href="#camp-report"/></svg>Shen Group FL</a></li>
                          <li><a href="../addreport/int/hl.php" class="link-dark rounded text-dark" target="main_frame">
                          <svg class="bi me-3" width="16" height="16"><use xlink:href="#camp-report"/></svg>Healthy Living</a></li>
                          <li><a href="../addreport/int/si.php" class="link-dark rounded text-dark" target="main_frame">
                          <svg class="bi me-3" width="16" height="16"><use xlink:href="#camp-report"/></svg>Squid Ink</a></li>
                          <li><a href="../addreport/int/pt.php" class="link-dark rounded text-dark" target="main_frame">
                          <svg class="bi me-3" width="16" height="16"><use xlink:href="#camp-report"/></svg>Patriots</a></li>
                          <li><a href="../addreport/int/bh.php" class="link-dark rounded text-dark" target="main_frame">
                          <svg class="bi me-3" width="16" height="16"><use xlink:href="#camp-report"/></svg>Beauty Health</a></li>
                          <li><a href="../addreport/int/ap.php" class="link-dark rounded text-dark" target="main_frame">
                          <svg class="bi me-3" width="16" height="16"><use xlink:href="#camp-report"/></svg>Affiliate Payments</a></li>
                        </ul>
                     </div>  
                    </li>

                    <li><a href="../addreport/int/lc.php" class="link-dark rounded text-dark" target="main_frame">
                          <svg class="bi me-3" width="16" height="16"><use xlink:href="#report"/></svg>LAE & CAC</a>
                    </li>



                </ul>
        </div>  
      </li>
      <hr>
      <li class="mb-1">
        <button class="btn btn-toggle align-items-center collapsed rounded  text-dark" data-bs-toggle="collapse" data-bs-target="#inb-collapse" aria-expanded="false">
                Inbound Mins
              </button>
              <div class="collapse scroll" id="inb-collapse">
                <ul class="btn-toggle-nav list-unstyled fw-normal pb-1 small">
                  <li><a href="../report/inb/dbl.php" class="link-dark rounded text-dark" target="main_frame">
                  <svg class="bi me-3" width="16" height="16"><use xlink:href="#campaign"/></svg>Dignity Bio Labs</a></li> 
                  <li><a href="../report/inb/sbb.php" class="link-dark rounded text-dark" target="main_frame">
                  <svg class="bi me-3" width="16" height="16"><use xlink:href="#campaign"/></svg>Saybyebugs</a></li>
                  <li><a href="../report/inb/stb.php" class="link-dark rounded text-dark" target="main_frame">
                  <svg class="bi me-3" width="16" height="16"><use xlink:href="#campaign"/></svg>Signal Tech Booster</a></li>
                  <li><a href="../report/inb/sa.php" class="link-dark rounded text-dark" target="main_frame">
                  <svg class="bi me-3" width="16" height="16"><use xlink:href="#campaign"/></svg>Safe Alarm</a></li>
                  <li><a href="../report/inb/aj.php" class="link-dark rounded text-dark" target="main_frame">
                  <svg class="bi me-3" width="16" height="16"><use xlink:href="#campaign"/></svg>Airjoi</a></li>
                  <li><a href="../report/inb/fn.php" class="link-dark rounded text-dark" target="main_frame">
                  <svg class="bi me-3" width="16" height="16"><use xlink:href="#campaign"/></svg>Fitnus</a></li>
                  <li><a href="../report/inb/fnb.php" class="link-dark rounded text-dark" target="main_frame">
                  <svg class="bi me-3" width="16" height="16"><use xlink:href="#campaign"/></svg>Fitnus Brace</a></li>
                  <li><a href="../report/inb/gb.php" class="link-dark rounded text-dark" target="main_frame">
                  <svg class="bi me-3" width="16" height="16"><use xlink:href="#campaign"/></svg>Glow Birds</a></li>
                  <li><a href="../report/inb/bl.php" class="link-dark rounded text-dark" target="main_frame">
                  <svg class="bi me-3" width="16" height="16"><use xlink:href="#campaign"/></svg>Bluvys</a></li>
                  <li><a href="../report/inb/dm.php" class="link-dark rounded text-dark" target="main_frame">
                  <svg class="bi me-3" width="16" height="16"><use xlink:href="#campaign"/></svg>Direct Media</a></li>
                  <li><a href="../report/inb/az.php" class="link-dark rounded text-dark" target="main_frame">
                  <svg class="bi me-3" width="16" height="16"><use xlink:href="#campaign"/></svg>Azurro</a></li>
                  <li><a href="../report/inb/is.php" class="link-dark rounded text-dark" target="main_frame">
                  <svg class="bi me-3" width="16" height="16"><use xlink:href="#campaign"/></svg>Identity Shield</a></li>
                  <li><a href="../report/inb/dc.php" class="link-dark rounded text-dark" target="main_frame">
                  <svg class="bi me-3" width="16" height="16"><use xlink:href="#campaign"/></svg>Dash Cam Pro</a></li>
                  <li><a href="../report/inb/tl.php" class="link-dark rounded text-dark" target="main_frame">
                  <svg class="bi me-3" width="16" height="16"><use xlink:href="#campaign"/></svg>Tai Lopez</a></li>
                  <li><a href="../report/inb/mb.php" class="link-dark rounded text-dark" target="main_frame">
                  <svg class="bi me-3" width="16" height="16"><use xlink:href="#campaign"/></svg>MentorBox</a></li>
                  <li><a href="../report/inb/hm.php" class="link-dark rounded text-dark" target="main_frame">
                  <svg class="bi me-3" width="16" height="16"><use xlink:href="#campaign"/></svg>Hotel Motel</a></li>
                  <li><a href="../report/inb/as.php" class="link-dark rounded text-dark" target="main_frame">
                  <svg class="bi me-3" width="16" height="16"><use xlink:href="#campaign"/></svg>Au Sante</a></li>
                  <li><a href="../report/inb/sl.php" class="link-dark rounded text-dark" target="main_frame">
                  <svg class="bi me-3" width="16" height="16"><use xlink:href="#campaign"/></svg>Skyline</a></li>
                </ul>
        </div>  
      </li>
      <hr>
       <li class="mb-1">
        <button class="btn btn-toggle align-items-center collapsed rounded  text-dark" data-bs-toggle="collapse" data-bs-target="#rev-collapse" aria-expanded="false">
                Rev Brand
              </button>
              <div class="collapse" id="rev-collapse">
                <ul class="btn-toggle-nav list-unstyled fw-normal pb-1 small">
                  <li><a href="../report/rev/fc.php" class="link-dark rounded text-dark" target="main_frame">
                  <svg class="bi me-3" width="16" height="16"><use xlink:href="#campaign"/></svg>Farmerscart</a></li>
                  <li><a href="../report/rev/fm.php" class="link-dark rounded text-dark" target="main_frame">
                  <svg class="bi me-3" width="16" height="16"><use xlink:href="#campaign"/></svg>Franklin Mint</a></li>
                  <li><a href="../report/rev/rs.php" class="link-dark rounded text-dark" target="main_frame">
                  <svg class="bi me-3" width="16" height="16"><use xlink:href="#campaign"/></svg>Radioshack</a></li>
                </ul>
        </div>  
      </li>
      <hr>
      <li class="mb-1">
        <button class="btn btn-toggle align-items-center collapsed rounded  text-dark" data-bs-toggle="collapse" data-bs-target="#av-collapse" aria-expanded="false">
                Address Verification
              </button>
              <div class="collapse" id="av-collapse">
                <ul class="btn-toggle-nav list-unstyled fw-normal pb-1 small">
                  <li><a href="../report/int/qi.php" class="link-dark rounded text-dark" target="main_frame">
                  <svg class="bi me-3" width="16" height="16"><use xlink:href="#campaign"/></svg>QUIC Industries</a></li>
                  <li><a href="../report/int/ts.php" class="link-dark rounded text-dark" target="main_frame">
                  <svg class="bi me-3" width="16" height="16"><use xlink:href="#campaign"/></svg>TJ&S</a></li>
                  <li><a href="../report/int/sg.php" class="link-dark rounded text-dark" target="main_frame">
                  <svg class="bi me-3" width="16" height="16"><use xlink:href="#campaign"/></svg>Shen Group</a></li>
                  <li><a href="../report/int/sgf.php" class="link-dark rounded text-dark" target="main_frame">
                  <svg class="bi me-3" width="16" height="16"><use xlink:href="#campaign"/></svg>Shen Group FL</a></li>
                  <li><a href="../report/int/hl.php" class="link-dark rounded text-dark" target="main_frame">
                  <svg class="bi me-3" width="16" height="16"><use xlink:href="#campaign"/></svg>Healthy Living</a></li>
                  <li><a href="../report/int/si.php" class="link-dark rounded text-dark" target="main_frame">
                  <svg class="bi me-3" width="16" height="16"><use xlink:href="#campaign"/></svg>Squid Ink</a></li>
                  <li><a href="../report/int/pt.php" class="link-dark rounded text-dark" target="main_frame">
                  <svg class="bi me-3" width="16" height="16"><use xlink:href="#campaign"/></svg>Patriots</a></li>
                  <li><a href="../report/int/bh.php" class="link-dark rounded text-dark" target="main_frame">
                  <svg class="bi me-3" width="16" height="16"><use xlink:href="#campaign"/></svg>Beauty Health</a></li>
                  <li><a href="../report/int/ap.php" class="link-dark rounded text-dark" target="main_frame">
                  <svg class="bi me-3" width="16" height="16"><use xlink:href="#campaign"/></svg>Affiliate Payments</a></li>
                </ul>
        </div>  
      </li>
      <hr>
      <li class="mb-1">
        <a href="../action/logout.php?logout" class="link-dark text-dark" style="text-decoration: none;"><button class="btn align-items-center collapsed text-dark">
                  <svg class="bi" width="17" height="17" ><use xlink:href="#logout"/></svg>&nbsp;Logout
        </button></a>
      </li>
    </ul>

  </div>

  <div class="b-example-divider"></div> 

  <iframe  src="image.php" height="auto" width="100%" name="main_frame"></iframe>
</main>


    <script src="../bootstrap/js/bootstrap.bundle.min.js"></script>

      <script src="../style/sidebars.js"></script>
  </body>
</html>

